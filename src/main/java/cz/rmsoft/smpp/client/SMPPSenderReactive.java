package cz.rmsoft.smpp.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Component
public class SMPPSenderReactive implements ISMPPSenderReactive {

    SMPPSenderDefault smppSenderDefault;

    @Autowired
    public SMPPSenderReactive(SMPPSenderDefault smppSenderDefault) {
        this.smppSenderDefault = smppSenderDefault;
    }

    @Override
    public Mono<ServerResponse> bind() {
        try {
            smppSenderDefault.bind();
            return ServerResponse.ok().build();
        } catch (Exception e) {
            return Mono.error(e);
        }
    }

    public Mono<ServerResponse> submit(final ServerRequest request) {
        String destinationAddress = request.queryParam("da").orElse("1111");
        String message = request.queryParam("message").orElse("Default message");
        String sender = request.queryParam("oa").orElse("0000");
        String senderTon = request.queryParam("ton").orElse("0");
        String senderNpi = request.queryParam("npi").orElse("1");
        try {
            if(!smppSenderDefault.bound) {
                return ServerResponse.ok().body(Mono.just("not bound"), String.class);
            }
            smppSenderDefault.submit(destinationAddress, message, sender, (byte)0, (byte)1);
            return ServerResponse.ok().body(Mono.just("Command successfully submitted"), String.class);
        } catch (Exception e) {
            return ServerResponse.badRequest().body(Mono.just("Failed to execute the command, " + e.getMessage()), String.class);
        }
    }

    @Override
    public Mono<ServerResponse> unbind() {
        try {
            smppSenderDefault.unbind();
            return ServerResponse.ok().build();
        } catch (Exception e) {
            return Mono.error(e);
        }
    }
}
