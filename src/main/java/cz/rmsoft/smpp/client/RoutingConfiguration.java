package cz.rmsoft.smpp.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import java.io.IOException;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;

@Configuration
@EnableWebFlux
public class RoutingConfiguration {

    @Bean
    public ISMPPSender ismppSender() throws IOException {
        return new SMPPSenderDefault();
    }

    @Bean
    @Autowired
    public RouterFunction<ServerResponse> myRouter(SMPPSenderReactive smppSenderReactive) throws IOException {
        return RouterFunctions.route(GET("/bind"), serverRequest -> {
            return smppSenderReactive.bind();
        }).andRoute(GET("/submit"), serverRequest -> {
            return smppSenderReactive.submit(serverRequest);
        }).andRoute(GET("/unbind"), serverRequest -> {
            return smppSenderReactive.unbind();
        });
    }

//    @Bean
//    public RouterFunction<ServerResponse> getRouter() {
//        return RouterFunctionFactory.getDefaultRouterConfiguration();
//    }

}
