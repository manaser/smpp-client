package cz.rmsoft.smpp.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SMPPSenderApplication {

	public static void main(String[] args) {
		SpringApplication.run(SMPPSenderApplication.class, args);
	}
}
