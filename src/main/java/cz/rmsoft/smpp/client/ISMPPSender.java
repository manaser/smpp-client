package cz.rmsoft.smpp.client;

import org.smpp.TimeoutException;
import org.smpp.WrongSessionStateException;
import org.smpp.pdu.PDUException;

import java.io.IOException;

public interface ISMPPSender {
    void bind() throws PDUException, TimeoutException, WrongSessionStateException, IOException;
    void loadProperties(String fileName) throws IOException;
    void submit(String destAddress, String shortMessage, String sender, byte senderTon, byte senderNpi);
    void unbind();
}
