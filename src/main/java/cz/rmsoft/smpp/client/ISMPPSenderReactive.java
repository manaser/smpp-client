package cz.rmsoft.smpp.client;

import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

public interface ISMPPSenderReactive {
    Mono<ServerResponse> bind();
    Mono<ServerResponse> submit(final ServerRequest request);
    Mono<ServerResponse> unbind();
}
