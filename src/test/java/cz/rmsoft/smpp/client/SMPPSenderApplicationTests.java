package cz.rmsoft.smpp.client;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SMPPSenderApplicationTests {

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void whenSubmitThenSomethingIsReturned() {
		String response = this.restTemplate.getForObject("/submit", String.class);
		//Assert.assertEquals("OK", response.toUpperCase());
	}

	@Test
	public void whenBindThenSomethingIsReturned() {
		String response = this.restTemplate.getForObject("/bind", String.class);
		//Assert.assertEquals("OK", response.toUpperCase());
	}

	@Test
	public void contextLoads() {
	}

}
